﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{

    public static float spacing = 2f;

    public static readonly Vector2[] directions =
    {
        new Vector2(spacing, 0),
        new Vector2(-spacing, 0),
        new Vector2(0, spacing),
        new Vector2(0, -spacing)
    };

}
